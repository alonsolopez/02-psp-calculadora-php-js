Algoritmo sin_titulo
	//variables de operacion
	Definir operando1 Como Caracter
	Definir operando2 Como Caracter
	Definir operador Como Caracter
	Definir resultado Como Real
	Definir contadorDecimales Como Entero
	//flags
	Definir OPERANDO1_CAPTURA Como Logico
	Definir OPERANDO2_CAPTURA Como Logico
	Definir OPERADOR_CAPTURA Como Logico
	Definir PUNTO_DECIMAL Como Logico
	Definir ESTADO Como Entero;
	
	//INICIO
	ESTADO=0;
	//o se aplasta numero, ., 
	Si Enter O btnIgual Y Estado=5
		resultado = operando1 operador operando2;
		
	FinSi
	SI leerEnTeclado = ^[0-9\.]+$ Entonces
		Si Estado = 0 Entonces
			Estado=1;
			resultado = btnCLick o caracter capturado.
		FinSi
		Si Estado=1 Entonces 
			//resultado = 1*10 + 3
			Si PUNTO_DECIMAL Entonces
				print resultado=(resultado) + (numeroPresionado/10*contadorDecimales);
				print (resultado)
				contadorDecimales = contadorDecimales+1;
			SiNo
				print resultado=(resultado*10) + numeroPresionado;
			FinSi
			
			Si PUNTO_DECIMAL=false Y se presiona '.' Entonces  (DEJAR SELECCIONADO BTN )
				PUNTO_DECIMAL=true;
				contadorDecimales=1;
				print ((resultado.replace('0', txtDisplay.lenght-2)) //34.0
					resultado = ConvertirAReal(resultado)
			SiNo
				"no hacer nada"
			FinSi
			Si click '+/-' Entonces 
				resultado = resultado * (-1);
				print resultado;
				Estado =2;
			FinSi
			Si click % Entonces
				resultado =  resultado /10;
				Si PUNTO_DECIMAL=false Entonces
					PUNTO_DECIMAL=true;
				FinSi
				Estado =2;
				print resultado;
			FinSi
			Si click OPERADOR Entonces
				Estado = 3;
				OPERANDO1_CAPTURA=true;
				operando1 = resultado;
				Si click btnDivision Entonces
					operador = "/"; // 0:"/", 1:"*", 2:"-", 3:"+"
					cambiar BORDER btnDivision
					ESTADO = 4;
				FinSi
				Si click btnMultiplicacion Entonces
					operador = "*"; // 0:"/", 1:"*", 2:"-", 3:"+"
					cambiar BORDER btnMultiplicacion
					ESTADO = 4;
				FinSi
			FinSi
		FinSi
		Si Estado = 2 Entonces
			btnClear.Text = 'AC';
			OPERANDO1_CAPTURA=true;
			Si click OPERADOR Entonces
				Estado = 3;
			FinSi
		FinSi
		Si ESTADO=3 Entonces
			Si click btnDivision Entonces
				operador = "/"; // 0:"/", 1:"*", 2:"-", 3:"+"
				cambiar BORDER btnDivision
				ESTADO = 4;
			FinSi
			Si click btnMultiplicacion Entonces
				operador = "*"; // 0:"/", 1:"*", 2:"-", 3:"+"
				cambiar BORDER btnMultiplicacion
				ESTADO = 4;
			FinSi
			
		FinSi
		
		Si Estado = 4 Y click OPERADOR Entonces
			Si click btnDivision Entonces
				operador = "/"; // 0:"/", 1:"*", 2:"-", 3:"+"
				cambiar BORDER btnDivision
				ESTADO = 4;
			FinSi
			Si click btnMultiplicacion Entonces
				operador = "*"; // 0:"/", 1:"*", 2:"-", 3:"+"
				cambiar BORDER btnMultiplicacion
				ESTADO = 4;
			FinSi
			OPERADOR_CAPTURA=true;
			Si btnClear.click Entonces
				OPERADOR_CAPTURA = false;
				btnSeleccionado quitar BORDER
			FinSi
			//limpiamos resultado SION IMPRIMIR EN EL DISPLAY
			resultado = 0;

		FinSi
		Si Estado=4 Y clickNumeros Entonces
			
			resultado = (resultado * 10) + btnNumero;
			print resultado;
		FinSi
		
		Si Estado=4 Y clickIgual o ENTER Entonces
			Estado = 5
		FinSi
		
	FinSi
	SI Estado= 5 Entonces
		Realizar operación
		Imprimir RESULTADO en DISPLAY
		reset FLags
	FinSi
	ESTADO=1
	//o se aplasta OPERADOR 
	ESTADO=3;
	
	
FinAlgoritmo
