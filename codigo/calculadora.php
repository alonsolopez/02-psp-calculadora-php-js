<?php
$op1 = 0;
$op2 = 0;
$ope = 0;
$res = 0;
//procesar los datos del submit
if (isset($_GET['enviado']))
{
    $op1 = isset($_GET['op1']) ? $_GET['op1'] : 0;
    $op2 = isset($_GET['op2']) ? $_GET['op2'] : 0;
    $ope = isset($_GET['ope']) ? $_GET['ope'] : '';
    //crear la instancia de Operacion
    $operacion = new Operacion($op1, $ope, $op2);
    var_dump("instancia");
    var_dump($operacion);
    $res = $operacion->realizar();
    var_dump("$ _GET");
    var_dump($_GET);
    var_dump("res=" . $res);
}

class Operacion
{

    //miembros
    public $operando1 = 0;
    public $operando2 = 0;
    public $operador = '';
    //constructor
    public function __construct($op1, $ope, $op2)
    {
        $this->operando1 = $op1;
        $this->operador  = $ope;
        $this->operando2 = $op2;
    }

    public function realizar()
    {
        switch ($this->operador)
        {
            case '/':
                return $this->operando1 / $this->operando2;
                break;
            case 'X':
            case 'x':
            case '*':
                echo " $this->operando1 * $this->operando2";
                echo "res=" . $this->operando1 * $this->operando2;
                // die("depurando calculadora");

                return $this->operando1 * $this->operando2;
                break;
            case '+':
                return $this->operando1 + $this->operando2;
                break;
            case '-':
                return $this->operando1 - $this->operando2;
                break;
        }
    }

    public function suma($op1, $op2)
    {
        return $op1 + $op2;
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>
        Calculadora</title>
    <link rel="stylesheet" href="./css/estilos.css">
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="./js/scripts.js"></script>
    <script>
    //vars
    var estado = 0;
    //miembros de la OPERACION
    var operando1 = 0;
    var operando2 = 0;
    var operador = '';


    //banderas de accones de calc
    var OPERANDO1_CAPTURA = false;
    var OPERANDO2_CAPTURA = false;
    var OPERANDOR_CAPTURA = false;
    var PUNTO_DECIMAL = false;


    //Teclado-----------------
    document.addEventListener("keydown", function(event) {
        //console.log('presionado: ' + event.which);
        //console.log('Keycode: ' + event.keyCode);
        //console.log('¿SHIFT?: ' + event.shiftKey);
        //console.log('valor: ' + event.key);
        //procesar keys presionadas...
        //nums
        switch (event.key) {
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                introNumero(event.key, true);
                break;
            case '*':
            case '/':
            case '+':
            case '-':
                seleccionOP(event.key, true);
                break;
            case '.':
                puntoDecimal();
                break;
            case '%':
                porciento();
                break;
            case 'Enter':
                igual();
                break;
            case 'Backspace':
                if (document.getElementById('txtDisplay').value != 0) {
                    document.getElementById('txtDisplay').value = document.getElementById('txtDisplay').value
                        .substr(0, document.getElementById('txtDisplay').value.length - 1);
                    console.log("long de la cadena: " + document.getElementById('txtDisplay').value.length);
                    console.log("display: ----|" + document.getElementById('txtDisplay').value + "|----");
                } else document.getElementById('txtDisplay').value = '0';
                if (document.getElementById('txtDisplay').value.length == 0) document.getElementById(
                    'txtDisplay').value = '0'
        }

    })

    function verificarEstado(clicked) {
        alert("boton ID=" + clicked.id + "/// value= " + clicked.value + "//// INNER HTML = " + clicked.innerHTML);
        //cambiar estado
        //resultado = ? ?

        document.getElementById('txtDisplay').value = (document.getElementById('txtDisplay').value * 10) + parseInt(
            clicked.innerHTML);
    }

    function introNumero(num, keys = false) {

        //si se presionó con teclado
        let valor = keys ? num : num.innerHTML;
        //casos por estado
        switch (estado) {
            case 3:
                estado = 4;
                //captrua operando2
                if (PUNTO_DECIMAL) document.getElementById('txtDisplay').value += valor;
                else
                    document.getElementById('txtDisplay').value = (document.getElementById('txtDisplay').value *
                        10) +
                    parseInt(valor);
                break;
            case 0:
                estado = 1;
                //cambia el texto 
                document.getElementById('btnAC').innerHTML = 'C';
            case 1:
            case 4:
                //captrua operando2
                if (PUNTO_DECIMAL) document.getElementById('txtDisplay').value += valor;
                else
                    document.getElementById('txtDisplay').value = (document.getElementById('txtDisplay').value *
                        10) +
                    parseInt(valor);

                break;
            case 2:
                ////limpiar todo
                //se toma el numero NUEVO, reemplaza a lo que haya en display
                if (PUNTO_DECIMAL) document.getElementById('txtDisplay').value = valor;
                else
                    document.getElementById('txtDisplay').value = parseInt(valor);
                //ponemos el btnAC en 'C'
                document.getElementById('btnAC').innerHTML = 'C';
                break;
        }
        ///verificar el num de chars en el display
        let display = document.getElementById('txtDisplay').value;
        console.log('num de chars en display = ' + display.length);
        //min 7 chars  -- font-size = 57
        //max 40 chars -- font-size = 57
        //alert(document.getElementById('txtDisplay').value);
        imprimirAConsola();
        //cambiar el font-size
        disminuirFuenteDisplay();
    }

    function disminuirFuenteDisplay() {
        // document.getElementById('txtDisplay').style.fontSize = "x-large";
        //alert('change');
        let display = document.getElementById('txtDisplay').value;
        console.log("fontsize original: " + document.getElementById('txtDisplay').style.cssText);

        let nuevoFontSize = (47 - (display.length <= 7 ? 0 : display.length - 7) / 0.5);
        //  document.getElementById('txtDisplay').style.cssText += " font-size: " + nuevoFontSize + "px;";
        console.log("fontSize=" + document.getElementById('txtDisplay').style.fontSize);

        switch (display.length) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
                document.getElementById('txtDisplay').style.cssText = " font-size: 47px;";
                break;
            case 8:
                document.getElementById('txtDisplay').style.cssText = " font-size: 45px;";
                break;
            case 9:
                document.getElementById('txtDisplay').style.cssText = " font-size: 40px;";
                document.getElementById('txtDisplay').style.fontSize = 1.02 + "em";
                break;
            case 10:
                document.getElementById('txtDisplay').style.cssText = " font-size: 35px;";
                break;
            case 11:
                document.getElementById('txtDisplay').style.cssText = " font-size: 30px;";
                break;
            case 12:
                document.getElementById('txtDisplay').style.cssText = " font-size: 20px;";
                break;

        }

    }

    function masMenos() {
        if (estado == 1) estado = 2;
        document.getElementById('txtDisplay').value = (document.getElementById('txtDisplay').value * -1);
        imprimirAConsola();
    }

    function porciento() {
        if (estado == 1) estado = 2;
        document.getElementById('txtDisplay').value = (document.getElementById('txtDisplay').value / 100);
        imprimirAConsola();
    }

    function limpiar() {
        //document.getElementById('txtDisplay').value = 0;
        //no es mismo MET...
        //reset(); //TODO: checar si no voltea ls valores de la operacion
        //solo limpìamos display
        document.getElementById('txtDisplay').value = '0';
        //cambiamos a AC, si estab el btnAC en C, o en AC
        document.getElementById('btnAC').innerHTML = 'AC';
        if (estado == 3) {
            //resetar el OPERADOR
            operador = '';
            estado = 2;

            document.getElementById('opPor').style.cssText = document.getElementById('opEntre').style.cssText =
                document.getElementById('opMas').style.cssText = document.getElementById('opMenos').style.cssText =
                '';
        } else if (estado != 0 && estado != 4) {
            //ya se limpio algo y se va aplastar de nuevo.. se tiene que resetar
            reset();
        }
        imprimirAConsola();

    }

    function puntoDecimal() {
        if (PUNTO_DECIMAL) {
            //YA ESTABA ACTIVADO
            //Hacemos BEEP
            beep();
        } else {
            PUNTO_DECIMAL = true;
            //incluirlo en display
            document.getElementById('txtDisplay').value += '.';
        }
    }



    function imprimirAConsola() {
        console.log('Display=' + document.getElementById('txtDisplay').value);

        console.log('FLAG PUNTO DECIMAL = ' + PUNTO_DECIMAL);
        console.log('FLAG OPERANDO1_CAPTURA=' + OPERANDO1_CAPTURA);
        console.log('FLAG OPERANDOR_CAPTURA=' + OPERANDOR_CAPTURA);
        console.log('FLAG OPERANDO2_CAPTURA=' + OPERANDO2_CAPTURA);

        console.log('Estado = ' + estado);
        console.log('Operando1=' + operando1);
        console.log('Operador = ' + operador);
        console.log('Operando2=' + operando2);
    }

    function reset() {
        document.getElementById('txtDisplay').value = '0';
        PUNTO_DECIMAL = OPERANDO1_CAPTURA = OPERANDOR_CAPTURA = OPERANDO2_CAPTURA = false;
        estado = operando1 = operando2 = 0;
        operador = '';
        document.getElementById('txtDisplay').style.cssText = " font-size: 47px";
    }


    function teclado(tecla) {
        alert(tecla);
        document.getElementById('txtDisplay').value = tecla;
    }

    function seleccionOP(op, keys = false) {
        // OPERADORES = 1 /,  2 *,  3 +,   4 -
        operador = keys ? op : op.innerHTML;
        //cambiar el estado = 3
        estado = 3;
        //interrumpe la captura del OPERANDO1
        operando1 = document.getElementById('txtDisplay').value;
        //cambiar style de OPERADOR
        switch (operador) {
            case '/':
                //poner border a opEnter 
                //limpiamos display
                document.getElementById('opEntre').style.cssText = 'outline:4px solid black;';
                //quitarlo a todos los demas
                document.getElementById('opPor').style.cssText = document.getElementById('opMas').style.cssText =
                    document.getElementById('opMenos').style.cssText = '';
                break;
            case 'X':
            case 'x':
            case '*':
                //poner border a opEnter 
                //limpiamos display
                document.getElementById('opPor').style.cssText = 'outline: 4px solid black;';
                //quitarlo a todos los demas
                document.getElementById('opEntre').style.cssText = document.getElementById('opMas').style.cssText =
                    document.getElementById('opMenos').style.cssText = '';
                break;
            case '+':
                //poner border a opEnter 
                //limpiamos display
                document.getElementById('opMas').style = 'outline: 4px solid black;';
                //quitarlo a todos los demas
                document.getElementById('opEntre').style = document.getElementById('opPor').style = document
                    .getElementById('opMenos').style = '';
                break;
            case '-':
                //poner border a opEnter 
                //limpiamos display
                document.getElementById('opMenos').style = 'outline: 4px solid black;';
                //quitarlo a todos los demas
                document.getElementById('opPor').style = document.getElementById('opMas').style = document
                    .getElementById('opEntre').style = '';
                break;
        }
        ///limpiamos display
        document.getElementById('txtDisplay').value = '0';
        //limpiamos la bandera de punto decimlal
        PUNTO_DECIMAL = false;
        //imprimir en consola los datos 
        console.log('Estado = ' + estado);
        console.log('Operando1=' + operando1);
        console.log('Operador = ' + operador);
        console.log('Operando2=' + operando2);
    }



    function igual() {
        ///tomamos OPERANDO2
        operando2 = document.getElementById('txtDisplay').value;
        document.getElementById('op1').value = operando1;
        document.getElementById('op2').value = operando2;
        document.getElementById('ope').value = operador;
        document.getElementById('enviado').value = true;
        document.getElementById('frmCalculadora').submit();
    }
    </script>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600&display=swap" rel="stylesheet">
</head>

<body>

    <form action="" method="get" id="frmCalculadora">

        <div class="container">
            <div class="canvas">
                <div class=" espacioDisplay ">
                    <input type="text " onchange="disminuirFuenteDisplay()" name="txtDisplay" maxlength="57 "
                        minlength="10 " size="9 " id="txtDisplay" class="display " value="<?php echo $res; ?>">
                    <script>
                    // document.getElementById('txtDisplay').style.fontSize = '7';
                    </script>
                </div>
                <div class="wrapperBotones " onkeypress="teclado(this) ">
                    <div class="box ac btn_negro " onclick="limpiar() " id="btnAC">AC</div>
                    <div class="box mas_menos btn_negro " onclick="masMenos();">+/-</div>
                    <div class="box porciento btn_negro " onclick="porciento();">%</div>
                    <div class="box  btn-naranja " id="opEntre" onclick="seleccionOP(this)">/</div>
                    <div class="box n7 btn-nums " onclick="introNumero(this) ">7</div>
                    <div class="box n8 btn-nums " onclick="introNumero(this) ">8</div>
                    <div class="box n9 btn-nums " onclick="introNumero(this) ">9</div>
                    <div class="box  btn-naranja " id="opPor" onclick="seleccionOP(this)">X</div>
                    <div class="box n4 btn-nums " onclick="introNumero(this) ">4</div>
                    <div class="box n5 btn-nums " onclick="introNumero(this) ">5</div>
                    <div class="box n6 btn-nums " onclick="introNumero(this) ">6</div>
                    <div class="box  btn-naranja " id="opMas" onclick=" seleccionOP(this) ">+</div>
                    <div class="box n1 btn-nums " onclick="introNumero(this) ">1</div>
                    <div class="box n2 btn-nums " onclick="introNumero(this) ">2</div>
                    <div class="box n3 btn-nums " onclick="introNumero(this) ">3</div>
                    <div class="box  btn-naranja " id="opMenos" onclick="seleccionOP(this) ">-</div>
                    <div class="box j btn-nums " onclick="introNumero(this) ">0</div>
                    <div class="box punto btn-nums " onclick="puntoDecimal(); ">.</div>
                    <div class="box  btn-naranja btn-igual " onclick="igual()">=</div>
                </div>
            </div>
            <input type="hidden" name="op1" id="op1" value="">
            <input type="hidden" name="op2" id="op2" value="">
            <input type="hidden" name="ope" id="ope" value="">
            <input type="hidden" name="enviado" id="enviado" value="false">
    </form>
</body>

</html>